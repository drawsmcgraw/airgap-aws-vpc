#
#resource "aws_iam_instance_profile" "tkg_deploy_profile" {
#  name = "tkg_deploy_instance_profile"
#  role = aws_iam_role.tkg_deploy_role.name
#}
#
#resource "aws_iam_role" "tkg_deploy_role" {
#  name = "tkg_deploy_role"
#  path = "/"
#
#  assume_role_policy = <<EOF
#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Effect": "Allow",
#      "Principal": {
#        "Service": [
#          "ec2.amazonaws.com"
#        ]
#      },
#      "Action": "sts:AssumeRole"
#    }
#  ]
#}
#
#EOF
#}

data "aws_partition" "current" {
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["capa-ami-ubuntu-18.04-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = [data.aws_partition.current.partition == "aws-us-gov" ? "412828410675" : "983417187201"]
}

# Uncomment and use if using a user-data script.
#data "template_file" "init" {
#  template = file(var.bootstrap_template_file)
#
#  vars = {
#    DEPLOYMENT_BUCKET = var.DEPLOYMENT_BUCKET
#    AWS_REGION        = var.AWS_REGION
#    S3_SERVICE        = "s3"
#  }
#}

module "tkg-ag-jumpbox" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.12.0"

  name           = "tkg-ag-jumpbox"
  instance_count = 1

  ami           = data.aws_ami.ubuntu.id
  instance_type = var.JUMPBOX_TYPE

  // need to figure out how to deal with vpc_security_group_ids and  subnets probs add them to outputs
  //vpc_security_group_ids = [module.air-gapped-vpc.default_security_group_id]
  vpc_security_group_ids = [var.security_group]
  //subnet_id              = module.air-gapped-vpc.private_subnets[0]
  subnet_id = var.vpc_private_subnets
  //TODO refactor references
  // will invoce this reference as the outputs of the docs

  key_name             = var.AWS_SSH_KEY_NAME
  #iam_instance_profile = aws_iam_instance_profile.tkg_deploy_profile.id

  # Uncomment this, and use the 'init' named 'template_file' above to use a 
  # user-data script. 
  # user_data = data.template_file.init.rendered

  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 80
    },
  ]

  tags = {
    Terraform   = "true"
    Environment = "tkg-demo"
  }
}




#resource "aws_iam_role_policy_attachment" "controller-attach" {
#  role       = aws_iam_role.tkg_deploy_role.name
#  policy_arn = data.aws_iam_policy.contorller-policy.arn
#}
#
#resource "aws_iam_role_policy_attachment" "node-attach" {
#  role       = aws_iam_role.tkg_deploy_role.name
#  policy_arn = data.aws_iam_policy.node-policy.arn
#}
#
#resource "aws_iam_role_policy_attachment" "cp-attach" {
#  role       = aws_iam_role.tkg_deploy_role.name
#  policy_arn = data.aws_iam_policy.cp-policy.arn
#}
#
#resource "aws_iam_role_policy_attachment" "ap-attach" {
#  role       = aws_iam_role.tkg_deploy_role.name
#  policy_arn = aws_iam_policy.additional_policy.arn
#}
#
#data "aws_iam_policy" "contorller-policy" {
#  name = "controllers.tkg.cloud.vmware.com"
#}
#data "aws_iam_policy" "node-policy" {
#  name = "nodes.tkg.cloud.vmware.com"
#}
#
#data "aws_iam_policy" "cp-policy" {
#  name = "control-plane.tkg.cloud.vmware.com"
#}
#
#data "aws_iam_policy" "ap-policy" {
#  name = aws_iam_policy.additional_policy.name
#}
#
#resource "aws_iam_policy" "additional_policy" {
#  name        = "additional_policy"
#  description = "TKG AG policy"
#
#  policy = <<EOF
#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Effect": "Allow",
#      "Action": [
#        "ec2:DescribeInstanceTypes",
#        "ec2:DescribeInstanceTypeOfferings"
#      ],
#      "Resource": "*"
#    }
#  ]
#}
#EOF
#}
