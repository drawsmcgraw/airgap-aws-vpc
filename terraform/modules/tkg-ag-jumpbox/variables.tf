# Uncomment and use if using a user-data script
#variable "DEPLOYMENT_BUCKET" {
#  type = string
#}

variable "AWS_REGION" {
  type = string
}

variable "JUMPBOX_TYPE" {
  type    = string
  default = "t3.large"
}

variable "AWS_SSH_KEY_NAME" {
  type = string
}

// new vars added to support a modularized approach
variable "security_group" {
  type = string
}

variable "vpc_private_subnets" {
  type = string
}

variable "bootstrap_template_file" {
  type = string
  default = "init.sh.tpl"
}
