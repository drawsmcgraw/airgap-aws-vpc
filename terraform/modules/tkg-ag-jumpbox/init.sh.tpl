#!/bin/bash
set -euxo pipefail
exec >> /root/install.out
exec 2>&1

#using root for the install
sudo su -
cd /root
export HOME=/root
sleep ${DELAY_START}m

if [ "${AWS_REGION}" = "us-east-1" ]; then
  curl -v -o deploy.tar https://${DEPLOYMENT_BUCKET}.s3.amazonaws.com/deploy.tar
else
  curl -v -o deploy.tar https://${DEPLOYMENT_BUCKET}.s3-${AWS_REGION}.amazonaws.com/deploy.tar
fi

#untar workspace
tar xvf deploy.tar
source current_config
source ./deploy/tkg-env.sh

#load/init cli
tar xvf tanzu-cli-bundle-v1.3.1-linux-amd64.tar
install cli/core/v1.3.1/tanzu-core-linux_amd64 /usr/local/bin/tanzu

#deploy dependencies
./deploy/install-ag-depends.sh current_config

source current_config
source ./deploy/tkg-env.sh
sudo sysctl net/netfilter/nf_conntrack_max=131072

tanzu plugin clean
tanzu plugin install --local cli all
timeout 8s tanzu management-cluster create || true
cp ./deploy/tkr-bom-v1.20.5+vmware.2-fips.1-tkg.1.yaml ~/.tanzu/tkg/bom
cp ./deploy/overlay/stig-overlay.yaml $HOME/.tanzu/tkg/providers/ytt/03_customizations
cp ./deploy/overlay/overlay-private.yaml $HOME/.tanzu/tkg/providers/ytt/03_customizations
cp ./deploy/overlay/registry_ca_cert.yaml $HOME/.tanzu/tkg/providers/ytt/03_customizations
tanzu management-cluster create -v 10

#remove bootsrtap credentials
sleep 10s
source ./deploy/utils.sh && use_instance_profile ./deploy/overlay

