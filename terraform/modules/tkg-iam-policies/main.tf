resource "aws_iam_role_policy_attachment" "controller-attach" {
  role       = aws_iam_role.tkg_deploy_role.name
  policy_arn = data.aws_iam_policy.contorller-policy.arn
}

resource "aws_iam_role_policy_attachment" "node-attach" {
  role       = aws_iam_role.tkg_deploy_role.name
  policy_arn = data.aws_iam_policy.node-policy.arn
}

resource "aws_iam_role_policy_attachment" "cp-attach" {
  role       = aws_iam_role.tkg_deploy_role.name
  policy_arn = data.aws_iam_policy.cp-policy.arn
}

resource "aws_iam_role_policy_attachment" "ap-attach" {
  role       = aws_iam_role.tkg_deploy_role.name
  policy_arn = aws_iam_policy.additional_policy.arn
}

data "aws_iam_policy" "contorller-policy" {
  name = "controllers.tkg.cloud.vmware.com"
}
data "aws_iam_policy" "node-policy" {
  name = "nodes.tkg.cloud.vmware.com"
}

data "aws_iam_policy" "cp-policy" {
  name = "control-plane.tkg.cloud.vmware.com"
}

data "aws_iam_policy" "ap-policy" {
  name = aws_iam_policy.additional_policy.name
}

resource "aws_iam_policy" "additional_policy" {
  name        = "additional_policy"
  description = "TKG AG policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeInstanceTypes",
        "ec2:DescribeInstanceTypeOfferings"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

/*resource "aws_iam_role" "tkg_deploy_role" {
  name = "tkg_deploy_role"
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}

EOF
}
*/