
module "air-gapped-vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name   = var.VPC_NAME
  cidr   = var.AWS_VPC_CIDR

  azs             = compact(tolist([var.AWS_NODE_AZ, var.AWS_NODE_AZ_1, var.AWS_NODE_AZ_2]))
  private_subnets = compact(tolist([var.AWS_PRIVATE_NODE_CIDR, var.AWS_PRIVATE_NODE_CIDR_1, var.AWS_PRIVATE_NODE_CIDR_2]))

  enable_dns_hostnames = true
  enable_dns_support   = true

  create_igw         = true
  enable_nat_gateway = false
  enable_vpn_gateway = false

  manage_default_route_table = true
  default_route_table_tags   = { DefaultRouteTable = true }

  manage_default_security_group = true
  default_security_group_ingress = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  default_security_group_egress = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  tags = {
    Terraform   = "true"
    Environment = "tkg-demo"
  }

  vpc_tags = {
    Name = var.VPC_NAME
  }
}
//TODO
// leaving this here for simplicities sake, can be refactored out at a later time.
module "vpc_endpoints" {
  source = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"

  vpc_id = module.air-gapped-vpc.vpc_id

  endpoints = {
    s3 = {
      service         = "s3"
      service_type    = "Gateway"
      route_table_ids = module.air-gapped-vpc.private_route_table_ids
    },
    elb = {
      service             = "elasticloadbalancing"
      private_dns_enabled = true
      security_group_ids  = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    ssm = {
      service             = "ssm"
      private_dns_enabled = true
      security_group_ids  = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    ssmmessages = {
      service             = "ssmmessages"
      private_dns_enabled = true
      security_group_ids  = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    ec2 = {
      service             = "ec2"
      private_dns_enabled = true
      security_group_ids  = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    ec2messages = {
      service             = "ec2messages"
      private_dns_enabled = true
      security_group_ids  = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    cloudformation = {
      service             = "cloudformation"
      private_dns_enabled = true
      security_group_ids  = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    sts = {
      service             = "sts"
      private_dns_enabled = true
      security_group_ids  = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    secretsmanager = {
      service             = "secretsmanager"
      private_dns_enabled = true
      security_group_ids  = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    }
  }
}