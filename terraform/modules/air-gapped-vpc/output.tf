output "ag_vpc_id" {
  description = "The ID of the VPC"
  value       = module.air-gapped-vpc.vpc_id
}


# Subnets
output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = module.air-gapped-vpc.private_subnets
}

output "default_security_group" {
  description = "the default security group"
  value = module.air-gapped-vpc.default_security_group_id
}