variable "AWS_NODE_AZ" {
  type = string
}

variable "AWS_NODE_AZ_1" {
  type = string
}

variable "AWS_NODE_AZ_2" {
  type = string
}


variable "AWS_PRIVATE_NODE_CIDR" {
  type = string
}

variable "AWS_PRIVATE_NODE_CIDR_1" {
  type = string
}

variable "AWS_PRIVATE_NODE_CIDR_2" {
  type = string
}

variable "VPC_NAME" {
  type = string
}

variable "AWS_VPC_CIDR" {
  type = string
}
