module "vpc_endpoints" {
  source = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"

  vpc_id = module.air-gapped-vpc.vpc_id

  endpoints = {
    s3 = {
      service         = "s3"
      service_type    = "Gateway"
      route_table_ids = module.air-gapped-vpc.private_route_table_ids
    },
    elb = {
      service="elasticloadbalancing"
      private_dns_enabled=true
      security_group_ids = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    ssm = {
        service="ssm"
        private_dns_enabled=true
        security_group_ids = [module.air-gapped-vpc.default_security_group_id]
        subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    ssmmessages = {
        service="ssmmessages"
        private_dns_enabled=true
        security_group_ids = [module.air-gapped-vpc.default_security_group_id]
        subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    ec2 = {
      service="ec2"
      private_dns_enabled=true
      security_group_ids = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    ec2messages= {
      service="ec2messages"
      private_dns_enabled=true
      security_group_ids = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    cloudformation= {
      service="cloudformation"
      private_dns_enabled=true
      security_group_ids = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    sts= {
      service="sts"
      private_dns_enabled=true
      security_group_ids = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    },
    secretsmanager= {
      service="secretsmanager"
      private_dns_enabled=true
      security_group_ids = [module.air-gapped-vpc.default_security_group_id]
      subnet_ids          = module.air-gapped-vpc.private_subnets
    }
  }
}