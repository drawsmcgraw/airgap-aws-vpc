module "complete-air-gapped-vpc" {
  source = "./modules/air-gapped-vpc"

  VPC_NAME                = var.VPC_NAME
  AWS_VPC_CIDR            = var.AWS_VPC_CIDR
  AWS_NODE_AZ             = var.AWS_NODE_AZ
  AWS_NODE_AZ_1           = var.AWS_NODE_AZ_1
  AWS_NODE_AZ_2           = var.AWS_NODE_AZ_2
  AWS_PRIVATE_NODE_CIDR   = var.AWS_PRIVATE_NODE_CIDR
  AWS_PRIVATE_NODE_CIDR_1 = var.AWS_PRIVATE_NODE_CIDR_1
  AWS_PRIVATE_NODE_CIDR_2 = var.AWS_PRIVATE_NODE_CIDR_2
}


module "tkg-ag-jumpbox" {
  source = "./modules/tkg-ag-jumpbox"

  AWS_REGION        = var.AWS_REGION
  JUMPBOX_TYPE      = var.JUMPBOX_TYPE
  AWS_SSH_KEY_NAME  = var.AWS_SSH_KEY_NAME

  security_group  = module.complete-air-gapped-vpc.default_security_group
  vpc_private_subnets     = module.complete-air-gapped-vpc.private_subnets[0]

  # Uncomment if using a user-data script
  # NOTE: You will need to rewrite this templated script. The original 
  #   content has been obviated.
  # bootstrap_template_file = "./modules/tkg-ag-jumpbox/init.sh.tpl"
  # DEPLOYMENT_BUCKET = var.DEPLOYMENT_BUCKET

}


#/*
#
#
#TODO
#module "vpc_endpoints"{
#
#}
#module "tkg-iam-policies" {
#  source = "./modules/tkg-iam-policies"
#}
#*/
