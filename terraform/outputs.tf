# VPC
output "ag_vpc_id" {
  description = "The ID of the VPC"
  value       = module.complete-air-gapped-vpc.ag_vpc_id
}

output "tkg-ag-jumpbox" {
  value = module.tkg-ag-jumpbox.tkg-ag-jumpbox
}

# Subnets
output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = module.complete-air-gapped-vpc.private_subnets
}





