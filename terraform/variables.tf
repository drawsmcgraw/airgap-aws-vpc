variable "VPC_NAME" {
  type    = string
  default = "tkg-ag-demo"
}

variable "AWS_REGION" {
  type = string
}

variable "AWS_SSH_KEY_NAME" {
  type = string
}

variable "AWS_VPC_CIDR" {
  type = string
}

variable "AWS_NODE_AZ" {
  type = string
}

variable "AWS_NODE_AZ_1" {
  type = string
}

variable "AWS_NODE_AZ_2" {
  type = string
}

variable "AWS_PRIVATE_NODE_CIDR" {
  type = string
}

variable "AWS_PRIVATE_NODE_CIDR_1" {
  type = string
}

variable "AWS_PRIVATE_NODE_CIDR_2" {
  type = string
}

variable "AWS_PUBLIC_NODE_CIDR" {
  type = string
}

variable "TKG_CUSTOM_IMAGE_REPOSITORY_FQDN" {
  type    = string
  default = "tkg-ag-jumpbox"
}

variable "JUMPBOX_TYPE" {
  type    = string
  default = "t3.large"
}

###variable "DEPLOYMENT_BUCKET" {
###  type = string
###}
###
