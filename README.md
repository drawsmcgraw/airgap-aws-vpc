# Airgapped VPC

These terraform files will:
- create a VPC
- create three subnets
- create AWS service endpoints (so you can still talk to AWS services)
- create a jumpbox in the VPC

## Using this repo

Configure your awscli client (Terraform will lean on that configuration).

Copy `terraform.tfvars.example` to `terraform.tfvars` and update values as needed.

Profit:

```
terraform init
terraform plan -out plan
terraform apply plan
```

## Connecting to the Jumpbox

To connect to your jumpbox, consider using [Session Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-sessions-start.html#sessions-start-ssh)

In order to use Session Manager, you will need to create an IAM policy and attach it to the instance (which these Terraform files do not do). Follow [this AWS doc](https://docs.aws.amazon.com/systems-manager/latest/userguide/setup-instance-profile.html) to create that policy.

## Enabling Internet Access

If, for reasons, you want to enable Internet access, you need to create an Internet Gateway, NAT Gateway, and make some route table changes. See [this section](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html) in the AWS docs for more.
